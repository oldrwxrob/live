---
Title: Viewer Video Requests
Subtitle: Yeah, Yeah, I'll Get to It
---

1. Command Line Searches with DuckDuck.go
1. TMUX Screen-centric Configuration 
1. Setting Up Fish / Asciiquarium
1. Review Vim Plugins

