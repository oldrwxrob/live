---
Title: Frequently Answered Questions
---

You ask. I answer, usually.

## Isn't this the same web site style as [rwx.gg](http://rwx.gg)?

Why yes, yes it is. Laziness is a virtue.

## What's your setup?

Here's my hardware and software applications that I rely on.

* Linux Mint Cinnamon
    * It just works.
    * Ubuntu/Debian.
* Alacritty Terminal
    * Uses OpenGL for rendering.
* Bash Shell
    * Universal, powerful.
    * Exported functions.
* TMUX 
    * Nested TMUX session
        * Two different sockets
    * Main session 
* WeeChat (not WeChat)
    * FIFO for comms.
