---
Title: RWXROB
NoTitle: true
NoTitleHead: true
Version: 0.0.1
---

<iframe src="https://embed.restream.io/player/index.html?token=645fb2e017f10407e51b8e7fc7e10e30" width="960" height="576" frameborder="0" allowfullscreen></iframe>

:::boxpoint
[rwxrob.live]{.spy .biggest}

[Welcome to the community!]{.bigger}  
I never intended the stream and community to blow up like it has, but I'm so happy it has, not for the usual reasons but because of the raw *good* it is providing for so many despite me. I witness people helping people, experienced veterans helping those getting their start to find their way. It's just so damn cool! Frankly, I'm just happy to be a part of it. Won't you join us?
:::

<style>

  main {
    max-width: unset;
  }
  
  main.content {
    text-align: center;
    margin: 0;
  }

  .boxpoint {
    font-family: var(--sans-font);
    font-size: 1.4em;
    text-align: center;
    line-height: 1.5em;
    max-width: 600px;
    display: inline-block;
    margin-top: 0;
  }

  .biggest { font-size: 1.3em; }
  .bigger { font-size: 1.15em; }
  .big { font-size: 1.02em; }
  .small { font-size: .9em; }
  .smaller { font-size: .8em; }
  .smallest { font-size: .7em;line-height:0.8; }

  footer {
    display: none;
  }

  a {
    color: none !important;
    text-decoration: none;
    background: none !important;
  }

  a:hover {
    text-decoration: dotted;
    background: none !important;
  }

</style>

