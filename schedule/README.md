---
Title: Weekly Streaming Schedule
NoTitle: true
---

---------------- -------------- -----------------------------------------
 Time               Day          Topic
---------------- -------------- -----------------------------------------
 11am-2pm         Weekdays        [Linux Beginner Boost](https://rwx.gg)

 Noon-1pm         Sundays         [Mentors Discord Coffee Talk](/coffee/)

 Any                Any           Live Coding / Writing / Hacking to Learn
---------------- -------------- -----------------------------------------

[**Eastern USA / New York (-0400)**  
(I live in North Carolina.)]{.center}

I code in Bash, Go, C, HTML, CSS, JavaScript and am learning Assembly, Rust, Haskell, and Erlang. (I hate Python, Java, and C++ so don't ask.) I write in Pandoc Markdown with YAML. I hack [HackTheBox](https://hackthebox.eu), [OverTheWire](https://overthewire.org), and [PicoCTF](https://picoctf.com) but when I hack I do it to learn and help others learn, not for speed (plenty of other streamers focus on speed).

:::co-warning
I've learned the hard way that it is physically impossible for me to get into a mental flow state while looking at chat so I often ignore it entirely except to take a break, but don't count on it. I regularly code for 2-3 hours at a time. I do usually verbalize what I'm doing so you can follow along if you wish. Moderators and community members are available in chat to help answer questions. Contact me through Discord or email if something is urgent needed.
:::
