# TODO List

Here's stuff we are working on together on the stream.

## TryHards

* Completely read every word on the <https://offensive-security.com> web site.
* Read all suggested preparation and reference materials listed on that site.

## Path to Pentesting

1. Basic (ground level) technical proficiency.
2. Preparing to take the OSCP exam.
3. Working toward "professional" pentesting opportunities.

